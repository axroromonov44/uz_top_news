import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import 'app.dart';
import 'core/app_global_state.dart';
import 'core/translations/codegen_loader.g.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await AppGlobalState().appSetUp();

  runApp(
    EasyLocalization(
      supportedLocales: const [
        Locale('ru', 'RU'),
        Locale('uz', 'UZ'),
        Locale('en', 'US'),
      ],
      path: 'lib/core/translations',
      assetLoader: const CodegenLoader(),
      fallbackLocale: const Locale('uz', 'UZ'),
      child: const App(),
    ),
  );
}
