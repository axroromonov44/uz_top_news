
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DependenciesProvider extends StatelessWidget {
  final Widget child;

  const DependenciesProvider({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        // BlocProvider(
        //   create: (BuildContext context) => AuthBloc(
        //       AuthRepository(AuthRemoteDataSource(), AuthLocalDataSource())),
        // ),
        // BlocProvider<ProfileBloc>(
        //     create: (context) => ProfileBloc(profileRepository:ProfileRepository()))
      ],
      child: child,
    );
  }
}
