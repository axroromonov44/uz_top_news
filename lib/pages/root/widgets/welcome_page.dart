import 'package:flutter/material.dart';

import '../../../core/config/theme/style.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: kBlueGradientVertical,
        ),
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text('Uz Top News',style: kBoldBlack25,)
                // Image.asset(
                //   'assets/images/bigsell_logo.png',
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
