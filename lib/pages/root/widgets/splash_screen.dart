import 'package:flutter/material.dart';

import '../../../core/config/theme/style.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    // WidgetsBinding.instance
    //     ?.addPostFrameCallback((_) async => chooseLanguage());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: const BoxDecoration(gradient: kBlueGradientVertical),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/splash_image.png',
              height: 100,
              width: 150,
            ),
            Text(
              'Продавай легко!',
              textAlign: TextAlign.center,
              style: kRegularBlack14.copyWith(color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}
