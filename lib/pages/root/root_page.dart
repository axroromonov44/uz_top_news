import 'package:flutter/material.dart';
import 'package:uz_top_news/pages/root/widgets/welcome_page.dart';

import 'main/main_page.dart';

class RootPage extends StatefulWidget {
  const RootPage({Key? key}) : super(key: key);

  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: Future.delayed(const Duration(seconds: 1)),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // return const SignIn();
            return const MainPage();
          }
          return const WelcomePage();
        },
      ),
    );
    ;
  }
}
