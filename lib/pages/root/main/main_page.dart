import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';

import '../../../core/config/theme/style.dart';
import '../../categories_page/presentation/categories_page.dart';
import '../../home_page/presentation/home_page.dart';
import '../../search_page/presentation/search_page.dart';

class MainPage extends StatefulWidget {
  const MainPage();

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int tabIndex = 0;

  @override
  void initState() {
    super.initState();
    //ToDo uncomment below to enable language picker window on first visit
    // WidgetsBinding.instance
    //     ?.addPostFrameCallback((_) async => chooseLanguage());
  }

  void _incrementTab(int index) {
    setState(() {
      tabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: const Color(0xff191826),
        title: const Text('Uz Top News'),
      ),
      body: IndexedStack(
        index: tabIndex,
        children: const [
          HomePage(),
          CategoriesPage(),
          SearchPage(),
        ],
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 2,
              blurRadius: 10,
            )
          ],
        ),
        child: BottomNavigationBar(
          backgroundColor: const Color(0xff191832),
          elevation: 0,
          unselectedItemColor: kGray,
          selectedItemColor: kWhite,
          selectedFontSize: 12,
          onTap: (int index) {
            setState(() {
              tabIndex = index;
            });
          },
          currentIndex: tabIndex,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          type: BottomNavigationBarType.fixed,
          items: const [
            BottomNavigationBarItem(
              label: "Home",
              icon: Icon(EvaIcons.homeOutline),
              activeIcon: Icon(EvaIcons.home),
            ),
            BottomNavigationBarItem(
              label: "Categories",
              icon: Icon(EvaIcons.gridOutline),
              activeIcon: Icon(EvaIcons.grid),
            ),
            BottomNavigationBarItem(
              label: "Search",
              icon: Icon(EvaIcons.searchOutline),
              activeIcon: Icon(EvaIcons.search),
            ),
          ],
        ),
      ),
      backgroundColor: kBackgroundColor,
    );
  }
}
