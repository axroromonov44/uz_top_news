import 'dart:io';

import 'package:dartz/dartz.dart';

import 'error/exception.dart';
import 'utils/constants/error_messages.dart';

Future<Either<String, T>> exceptionHandler<T>(
  Future<Either<String, T>> Function() tryThis,
) async {
  try {
    return await tryThis();
  } on ServerException {
    return Left(serverErrorMsg);
  } on CacheException {
    return Left(cacheErrorMsg);
  } on SocketException {
    return Left(noInternetErrorMsg);
  }
}
