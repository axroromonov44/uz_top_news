import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';
import 'package:uz_top_news/core/config/theme/style.dart';

class CustomTextField extends StatelessWidget {
  final List<TextInputFormatter>? inputFormatters;
  final TextInputAction? textInputAction;

  // final TextCapitalization textCapitalization;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final String? initialValue;
  final bool autoFocus;
  final bool obscureText;

  final bool readOnly;
  final bool isUnderlined;
  final String? hintText;
  final int? hintMaxLines;
  final TextStyle? hintStyle;
  final String? errorText;
  final Function(String)? onChanged;
  final TextInputType? keyboardType;
  final TextEditingController? controller;
  final TextEditingValue? value;
  final FormFieldValidator<String>? validator;
  final Color? borderColor;
  final Function(String)? getValue;
  final int? maxLength;
  final EdgeInsets? contentPadding;
  final Color? color;

  const CustomTextField({
    Key? key,
    this.inputFormatters,
    this.autoFocus = false,
    this.readOnly = false,
    this.obscureText = false,
    this.initialValue,
    // this.textCapitalization,
    this.textInputAction,
    this.suffixIcon,
    this.hintText,
    this.hintStyle,
    this.errorText,
    this.onChanged,
    this.keyboardType,
    this.controller,
    this.validator,
    this.value,
    this.hintMaxLines,
    this.isUnderlined = true,
    this.borderColor,
    this.getValue,
    this.maxLength,
    this.prefixIcon,
    this.contentPadding,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return !isUnderlined
        ? Container(
            decoration: BoxDecoration(
                border: borderColor != null
                    ? Border.all(color: borderColor!)
                    : null,
                borderRadius: BorderRadius.circular(12),
                color: color ?? kLightGrey),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: TextFormField(
                obscureText: obscureText,
                // textCapitalization: textCapitalization,
                validator: validator,
                initialValue: initialValue,
                controller: controller,
                keyboardType: keyboardType,
                textInputAction: textInputAction ?? TextInputAction.next,
                inputFormatters: inputFormatters,
                onChanged: (value) {
                  if (getValue != null) getValue!(value);
                },
                autofocus: autoFocus,
                readOnly: readOnly,
                maxLines: hintMaxLines,
                decoration: InputDecoration(
                  contentPadding: contentPadding,
                  hintMaxLines: hintMaxLines,
                  hintText: hintText,
                  hintStyle: hintStyle ??
                      const TextStyle(
                        fontSize: 14,
                        color: kGray,
                      ),
                  errorText: errorText,
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  suffixIcon: suffixIcon,
                  prefixIcon: prefixIcon,
                ),
              ),
            ),
          )
        : TextFormField(
          obscureText: obscureText,
          // textCapitalization: textCapitalization,
          validator: validator,
          initialValue: initialValue,
          controller: controller,
          keyboardType: keyboardType,
          textInputAction: textInputAction,
          inputFormatters: inputFormatters,
          onChanged: (value) {
            if (getValue != null) getValue!(value);
          },
          autofocus: autoFocus,
          readOnly: readOnly,
          maxLength: maxLength,
          decoration: InputDecoration(
              filled: true,
              fillColor: kBackgroundColor,
              contentPadding: contentPadding,
              hintMaxLines: hintMaxLines,
              hintText: hintText,
              hintStyle: hintStyle ??
                  const TextStyle(
                    fontSize: 14,
                    color: Color.fromRGBO(189, 189, 189, 1),
                  ),
              errorText: errorText,
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide: const BorderSide(color: Colors.red)),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(12.0),
                borderSide: const BorderSide(
                  color: kLightGrey,
                ),
              ),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(16)),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(12.0),
                borderSide: const BorderSide(
                  color: kLightGrey,
                ),
              ),
              suffixIcon: suffixIcon,
              prefixIcon: prefixIcon),
        );
  }
}
