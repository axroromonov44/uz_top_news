import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductGridItem extends StatelessWidget {
  final String imageUrl;
  final double price;
  final String name;
  final double? offset;
  final double? oldPrice;
  final GestureTapCallback? onTap;

  const ProductGridItem(this.imageUrl, this.price, this.name,
      {this.offset, this.oldPrice, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        child: Column(
          children: [
            Image.network(
              imageUrl,
              frameBuilder: (BuildContext context, Widget child, int? frame,
                  bool wasSynchronouslyLoaded) {
                if (wasSynchronouslyLoaded) {
                  return child;
                }
                return AnimatedOpacity(
                  opacity: frame == null ? 0 : 1,
                  duration: const Duration(seconds: 1),
                  curve: Curves.easeOut,
                  child: child,
                );
              },
              loadingBuilder: (BuildContext context, Widget child,
                  ImageChunkEvent? loading) {
                if (loading == null) {
                  return child;
                }
                return const SizedBox(
                  height: 140,
                  width: 140,
                  child: Center(
                      child: CupertinoActivityIndicator(
                    animating: true,
                  )),
                );
              },
              fit: BoxFit.fill,
            ),
            if (name != null && name.isNotEmpty) Text(name),
            if (price != null) Text("UZS $price")
          ],
        ),
      ),
    );
  }
}
