import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../config/theme/style.dart';
import '../../config/theme/ui_helpers.dart';
import '../retry_button.dart';

class ErrorPage extends StatelessWidget {
  final VoidCallback onTap;
  final String errorMessage;
  final String? errorImageUri;

  const ErrorPage({
    Key? key,
    required this.onTap,
    required this.errorMessage,
    this.errorImageUri,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              errorImageUri ?? 'assets/images/no_connection_image.png',
              height: 30.h,
              width: 60.w,
            ),
            Text(
              errorMessage,
              style: kRegularBlack14,
            ),
            verticalSpace20,
            RetryButton(onTap: onTap),
          ],
        ),
      ),
    );
  }
}
