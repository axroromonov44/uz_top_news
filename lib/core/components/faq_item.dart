// import 'package:expandable/expandable.dart';
// import 'package:flutter/material.dart';
// import 'package:caliph_seller/core/config/theme/style.dart';
//
// class FaqItemWidget extends StatelessWidget {
//   final FaqModel? faqModel;
//
//   const FaqItemWidget({Key? key, this.faqModel}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.fromLTRB(16, 10, 0, 8),
//       child: ExpandablePanel(
//         header: Padding(
//           padding: const EdgeInsets.only(top: 10),
//           child: Text(
//             faqModel?.title ?? '',
//             style: kBoldBlack16,
//             maxLines: 2,
//             overflow: TextOverflow.ellipsis,
//           ),
//         ),
//         collapsed: Text(''),
//         expanded: Padding(
//           padding: const EdgeInsets.only(right: 30),
//           child: Text(
//             faqModel?.body ?? 'This is the big text',
//             softWrap: true,
//             style: kRegularBlack14,
//           ),
//         ),
//         theme: const ExpandableThemeData(
//             expandIcon: Icons.chevron_right_sharp,
//             iconRotationAngle: 2,
//             collapseIcon: Icons.keyboard_arrow_up_sharp,
//             collapsedIconColor: Color.fromRGBO(254, 76, 76, 1),
//             expandedIconColor: Color.fromRGBO(51, 51, 51, 1)),
//       ),
//     );
//   }
// }
