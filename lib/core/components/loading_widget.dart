import 'package:flutter/material.dart';

import '../config/theme/style.dart';

class BuildLoading extends StatelessWidget {
  const BuildLoading();

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(
        color: kBlue,
      ),
    );
  }
}
