import 'package:flutter/material.dart';

class ConstrainedScrollView extends StatelessWidget {
  final Widget child;
  final ScrollPhysics? scrollPhysics;
  final bool? reverse;
  final Axis? scrollDirection;

  const ConstrainedScrollView({
    required this.child,
    this.scrollPhysics,
    this.reverse,
    this.scrollDirection,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return SingleChildScrollView(
          scrollDirection: scrollDirection ?? Axis.vertical,
          physics: scrollPhysics ?? const ClampingScrollPhysics(),
          reverse: reverse ?? true,
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: constraints.maxWidth,
              minHeight: constraints.maxHeight,
            ),
            child: IntrinsicHeight(child: child),
          ),
        );
      },
    );
  }
}
