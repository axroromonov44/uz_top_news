
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import 'package:uz_top_news/core/config/theme/style.dart';
import 'package:uz_top_news/core/config/theme/ui_helpers.dart';

class CustomRadioButton extends StatelessWidget {
  final String? image;
  final String? label;
  final String? subTitle;
  final String? title;
  final dynamic groupValue;
  final dynamic value;
  final Function? onChanged;

  const CustomRadioButton({
    Key? key,
    this.image,
    this.label,
    this.subTitle,
    this.title,
    this.groupValue,
    this.value,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (image != null) ...[
              SvgPicture.asset(image!),
            ],
            horizontalSpace16,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                      text: label,
                      style: kRegularBlack16,
                      recognizer: TapGestureRecognizer()..onTap = () {}),
                ),
                verticalSpace10,
                if (title != null) ...[
                  SizedBox(
                    width: 65.w,
                    child: RichText(
                      text: TextSpan(
                          text: title,
                          style: kRegularBlack16,
                          recognizer: TapGestureRecognizer()..onTap = () {}),
                    ),
                  ),
                ],
                verticalSpace10,
                if (subTitle != null) ...[
                  SizedBox(
                    width: 65.w,
                    child: RichText(
                      text: TextSpan(
                          text: subTitle,
                          style: kBoldBlack16,
                          recognizer: TapGestureRecognizer()..onTap = () {}),
                    ),
                  ),
                ]
              ],
            ),
          ],
        ),
        const Spacer(),
        Radio<dynamic>(
            activeColor: kRed,
            groupValue: groupValue,
            value: value,
            onChanged: (newValue) {
              if (onChanged != null) {
                onChanged!(newValue);
              }
            }),
      ],
    );
  }
}
