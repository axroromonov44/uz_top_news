
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../config/theme/style.dart';
import '../config/theme/ui_helpers.dart';

class MainButton extends StatelessWidget {
  final void Function() onPressed;
  final String title;
  final EdgeInsetsGeometry? padding;
  final TextStyle? textStyle;
  final bool pressable;
  final bool showArrow;
  final bool outlinedBorder;
  final double height;
  final double? width;
  final String? image;
  final Color? color;
  final Color? imgColor;

  const MainButton({
    Key? key,
    required this.onPressed,
    required this.title,
    this.color,
    this.imgColor,
    this.padding,
    this.pressable = true,
    this.showArrow = false,
    this.outlinedBorder = false,
    this.height = 50,
    this.width,
    this.image,
    this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: pressable ? onPressed : null,
      child: Container(
        padding: padding,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: color ?? kBlue, width: 1.5),
            color: outlinedBorder
                ? kWhite
                : pressable
                    ? color ?? kBlue
                    : kGreyButton),
        height: height,
        width: width,
        child: Center(
          child: showArrow
              ? Row(
                  children: [
                    Text(
                      title,
                      style: textStyle ??
                          kRegularBlack14.copyWith(color: kWhite),
                    ),
                    horizontalSpace8,
                    const Image(
                      image: AssetImage('assets/icons/arrow_forward.png'),
                    )
                  ],
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (image != null) ...[
                      SvgPicture.asset(image!,
                          color: imgColor??kWhite),
                      horizontalSpace8,
                    ],
                    Text(title,
                        style: textStyle ??
                            kRegularBlack12.copyWith(
                                color: outlinedBorder ? kBlue : kWhite)),
                  ],
                ),
        ),
      ),
    );
  }
}
