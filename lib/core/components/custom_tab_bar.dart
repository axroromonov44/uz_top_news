import 'package:flutter/material.dart';

import '../config/theme/style.dart';

class CustomTabBar extends StatelessWidget {
  const CustomTabBar({
    Key? key,
    required this.tabTitles,
    required this.tabController,
    required this.onTabTapped,
    required this.unselectedLabelColor,
    required this.labelColor,
    this.withDivider = false,
    this.isScrollable = false,
    this.color,
    this.tabIndex,
    this.hasIndicator = true,
  }) : super(key: key);

  final bool withDivider;
  final bool isScrollable;
  final List<String> tabTitles;
  final TabController tabController;
  final Function(int) onTabTapped;
  final Color? unselectedLabelColor;
  final Color? labelColor;
  final Color? color;
  final int? tabIndex;
  final bool hasIndicator;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      color: color,
      child: Column(
        children: [
          TabBar(
            isScrollable: isScrollable,
            controller: tabController,
            labelColor: labelColor,
            unselectedLabelColor: unselectedLabelColor,
            labelStyle: kBoldBlack16,
            unselectedLabelStyle: kRegularBlack14,
            labelPadding: const EdgeInsets.all(10),
            indicatorColor: kBlue,
            indicatorWeight: 3,
            indicator: hasIndicator
                ? BoxDecoration(
                    color: tabIndex != null && tabIndex == 0 ? kYellow : kBlue,
                    borderRadius: BorderRadius.circular(5),
                  )
                : null,
            onTap: onTabTapped,
            tabs: [
              for (int i = 0; i < tabTitles.length; i++)
                Container(
                    padding: const EdgeInsets.only(right: 10),
                    height: 20,
                    child: Tab(text: tabTitles[i])),
            ],
          ),
          if (withDivider)
            const Divider(
              thickness: 1,
              height: 1,
              color: Color.fromRGBO(224, 224, 224, 1),
            ),
        ],
      ),
    );
  }
}
