
import 'package:flutter/material.dart';

import '../config/theme/style.dart';
import '../utils/constants/api.dart';

class CustomCircleAvatar extends StatelessWidget {
  final double radius;
  final String? imageUrl;

  ///for default image(error image)
  final bool? isThisSeller;

  const CustomCircleAvatar({
    Key? key,
    this.radius = 20,
    this.imageUrl,
    this.isThisSeller = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: kWhite,
      radius: radius,
      child: ClipOval(
        child: imageUrl != null
            ? Image.network(
                '${API.DownloadIMG}${imageUrl!}',
                fit: BoxFit.cover,
                height: radius * 2,
                width: radius * 2,
                errorBuilder: (context, error, stackTrace) {
                  return Image.asset(
                    isThisSeller == true
                        ? 'assets/images/store_default_image.png'
                        : 'assets/images/guest_avatar.png',
                    fit: BoxFit.cover,
                    height: radius * 2,
                    width: radius * 2,
                  );
                },
              )
            : Image.asset(
                isThisSeller == true
                    ? 'assets/images/store_default_image.png'
                    : 'assets/images/guest_avatar.png',
                fit: BoxFit.cover,
                height: radius * 2,
                width: radius * 2,
              ),
      ),
    );
  }
}
