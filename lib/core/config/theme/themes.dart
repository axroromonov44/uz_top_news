import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uz_top_news/core/config/theme/style.dart';

ThemeData lightTheme = ThemeData(
  fontFamily: 'Poppins',
  brightness: Brightness.light,
  appBarTheme: const AppBarTheme(color: Color(0xffF6F6FA)),
  primaryColor: const Color(0xffCBAB97),
  primaryColorBrightness: Brightness.light,
  accentColor: const Color(0xffCBAB97),
  backgroundColor: const Color(0xffF6F6FA),
  scaffoldBackgroundColor: kBackgroundColor,
  cardColor: const Color(0xffFFFFFF),
  buttonColor: const Color(0xff333333),
  focusColor: const Color(0xffCBAB97),
  primaryIconTheme: const IconThemeData(color: Color(0xff1E2D55)),
  bottomAppBarColor: const Color(0xffCBAB97),
  cupertinoOverrideTheme: const CupertinoThemeData(
    barBackgroundColor: Color(0xffECECEF),

  ),
);

ThemeData darkTheme = ThemeData(
    fontFamily: 'Poppins',
    brightness: Brightness.dark,
    primaryColorBrightness: Brightness.dark,
    backgroundColor: const Color(0xff101B39),
    scaffoldBackgroundColor: const Color(0xff101B39),
    accentColor: const Color(0xffCBAB97),
    cardColor: const Color(0xff101B39),
    primaryIconTheme: const IconThemeData(color: Color(0xffF6F6FA)),
    bottomSheetTheme:
        const BottomSheetThemeData(modalBackgroundColor: Color(0xff101B39)),
    buttonColor: const Color(0xffF6F6FA),
    focusColor: const Color(0xffCBAB97),
    cupertinoOverrideTheme: const CupertinoThemeData(
      barBackgroundColor: Color(0xff202D51),
    ));

TextTheme textTheme = const TextTheme(
  headline1: TextStyle(
      fontSize: 24, fontWeight: FontWeight.bold, color: Color(0xffCBAB97)),
  headline2: TextStyle(
      fontSize: 20, color: Color(0xffCBAB97), fontWeight: FontWeight.w600),
  bodyText2: TextStyle(fontSize: 14.0),
);
