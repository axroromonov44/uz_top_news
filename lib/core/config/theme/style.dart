import 'package:flutter/material.dart';

const Color kGray = Color.fromRGBO(150, 171, 186, 1);
const Color kGray6 = Color.fromRGBO(242, 242, 242, 1);
const Color kGreen = Color.fromRGBO(39, 174, 96, 1);
const Color kBlack = Color.fromRGBO(64, 64, 64, 1);
  const Color kWhite = Color.fromRGBO(255, 255, 255, 1);
const Color kBlue8 = Color.fromRGBO(68, 160, 235, 0.8);
const Color kBlue = Color.fromRGBO(68, 160, 235, 1);
const Color kGreyButton = Color.fromRGBO(203, 213, 221, 1);
const Color kRed = Color.fromRGBO(255, 12, 85, 1);
const Color kBackgroundColor = Color.fromRGBO(248, 250, 254, 1);
const Color kAddressColor = Color.fromRGBO(233, 240, 255, 1);
const Color kYellow = Color.fromRGBO(242, 201, 76, 1);
const Color kLightGrey = Color.fromRGBO(241, 244, 253, 1);
const Color kPurpleColor = Color.fromRGBO(255, 169, 195, 1);
const Color kOrange = Color.fromRGBO(242, 153, 74, 1);

const kBoldWhite14 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 14,
    color: kWhite,
    fontFamily: "Ubuntu");
const kBoldBlack25 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 25,
    color: kBlack,
    fontFamily: "Ubuntu");
const kBoldBlack20 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 20,
    color: kBlack,
    fontFamily: "Ubuntu");
const kBoldBlack18 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 18,
    color: kBlack,
    fontFamily: "Ubuntu");
const kBoldBlack16 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 16,
    color: kBlack,
    fontFamily: "Ubuntu");
const kBoldBlack14 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 14,
    color: kBlack,
    fontFamily: "Ubuntu");
const kBoldBlack12 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 12,
    color: kBlack,
    fontFamily: "Ubuntu");

const kMediumBlack20 = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 20.0,
    color: kBlack,
    fontFamily: "Ubuntu");
const kMediumBlack18 = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 18.0,
    color: kBlack,
    fontFamily: "Ubuntu");
const kMediumBlack16 = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 16.0,
    color: kBlack,
    fontFamily: "Ubuntu");
const kMediumBlack14 = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 14.0,
    color: kBlack,
    fontFamily: "Ubuntu");
const kMediumBlack12 = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 12.0,
    color: kBlack,
    fontFamily: "Ubuntu");

const kRegularBlack25 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 25.0,
    color: kBlack,
    fontFamily: "Ubuntu");
const kRegularBlack20 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 20.0,
    color: kBlack,
    fontFamily: "Ubuntu");
const kRegularBlack18 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 18.0,
    color: kBlack,
    fontFamily: "Ubuntu");
const kRegularBlack16 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
    color: kBlack,
    fontFamily: "Ubuntu");
const kRegularBlack14 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 14.0,
    color: kBlack,
    fontFamily: "Ubuntu");
const kRegularBlack12 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 12.0,
    color: kBlack,
    fontFamily: "Ubuntu");
const kRegularBlack10 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 10.0,
    color: kBlack,
    fontFamily: "Ubuntu");

const kBoldGrey20 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 20,
    color: kGray,
    fontFamily: "Ubuntu");
const kBoldGrey18 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 18,
    color: kGray,
    fontFamily: "Ubuntu");
const kBoldGrey16 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 16,
    color: kGray,
    fontFamily: "Ubuntu");
const kBoldGrey14 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 14,
    color: kGray,
    fontFamily: "Ubuntu");
const kBoldGrey12 = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 12,
    color: kGray,
    fontFamily: "Ubuntu");

const kRegularGrey20 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 20.0,
    color: kGray,
    fontFamily: "Ubuntu");
const kRegularGrey18 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 18.0,
    color: kGray,
    fontFamily: "Ubuntu");
const kRegularGrey16 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
    color: kGray,
    fontFamily: "Ubuntu");
const kRegularGrey14 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 14.0,
    color: kGray,
    fontFamily: "Ubuntu");
const kRegularGrey12 = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 12.0,
    color: kGray,
    fontFamily: "Ubuntu");

const kMediumGrey20 = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 20.0,
    color: kGray,
    fontFamily: "Ubuntu");
const kMediumGrey18 = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 18.0,
    color: kGray,
    fontFamily: "Ubuntu");
const kMediumGrey16 = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 16.0,
    color: kGray,
    fontFamily: "Ubuntu");
const kMediumGrey14 = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 14.0,
    color: kGray,
    fontFamily: "Ubuntu");
const kMediumGrey12 = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 12.0,
    color: kGray,
    fontFamily: "Ubuntu");

const Gradient kBlueGradientHorizontal = LinearGradient(
  colors: [
    Color.fromRGBO(48, 187, 220, 1),
    Color.fromRGBO(8, 160, 235, 1),
  ],
);
const Gradient kOrangeGradient = LinearGradient(
  colors: [
    Color.fromRGBO(250, 133, 29, 1),
    Color.fromRGBO(255, 152, 61, 1),
  ],
);
const Gradient kYellowGradient = LinearGradient(
  colors: [
    Color.fromRGBO(242, 201, 76, 1),
    Color.fromRGBO(234, 186, 41, 1),
  ],
);

const Gradient kBlueGradientVertical = LinearGradient(
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  colors: [
    Color.fromRGBO(48, 187, 220, 1),
    Color.fromRGBO(68, 160, 235, 1),
  ],
);
