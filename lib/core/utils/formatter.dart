///removes all empty spaces
String formatPhoneNumber(String phoneNumber) {
  return "998$phoneNumber".replaceAll(' ', '');
}

///takes "2021-10-14T05:38:30.919Z" returns 14.10.2021
String dateFormatter(DateTime datetime) {
  return "${datetime.day}.${datetime.month}.${datetime.year}";
}

///takes "2021-10-14T05:38:30.919Z" returns 14.10.2021, 5:38
String dateTimeFormatter(DateTime dateTime) {
  return "${dateTime.day}.${dateTime.month}.${dateTime.year}, ${dateTime.hour}:${dateTime.minute}";
}

/// Takes 2020-12 and returns 12/2020
String parseDateFromJson(String dateTime) {
  String date;
  date = "${dateTime.split("-").last}-${dateTime.split("-").first}";
  return date;
}

String parseDiscountEndDateToDateTime(DateTime dateTime) {
  return "${dateTime.toIso8601String()}Z";
}
