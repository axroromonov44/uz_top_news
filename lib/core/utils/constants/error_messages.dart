import 'package:easy_localization/easy_localization.dart';
import 'package:uz_top_news/core/translations/locale_keys.g.dart';

final String serverErrorMsg = LocaleKeys.server_failure.tr();
final String cacheErrorMsg = LocaleKeys.cache_failure.tr();
final String noInternetErrorMsg = LocaleKeys.noConnection.tr();
