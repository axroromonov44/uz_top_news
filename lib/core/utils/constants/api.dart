class API {
  // static const bool useProd = kReleaseMode;
  static const bool useProd =
      true; //to check with production url even in debug mode

  static const String BASE_URL = useProd
      ? 'https://api-gateway.caliphbaba.uz/api'
      : 'http://caliphbaba.lc:8080/api';

  static const String URL7777 = 'http://bgs098.zucco.tech:8080/api';
  static const String URL8003 = 'http://bgs098.zucco.tech:8080/api';
  static const String URL9002 = 'http://bgs098.zucco.tech:9002/api';
  static const String URL8004 = 'http://bgs098.zucco.tech:8080/api';
  static const String URL8005 = 'http://bgs098.zucco.tech:8080/api';
  static const String URL8006 = 'http://bgs098.zucco.tech:8080/api';
  static const String URL8007 = 'http://bgs098.zucco.tech:8080/api';
  static const String URL8008 = 'http://bgs098.zucco.tech:8080/api';
  static const String URL8009 = 'http://bgs098.zucco.tech:8080/api';
  static const String URL8080 = 'http://bgs098.zucco.tech:8080/api';
  static const String DownloadFile =
      'http://bgs098.zucco.tech:8080/api/file/v1/download?file=';
  static const String DownloadProductIMG =
      'http://bgs098.zucco.tech:8080/api/file/v1/product/image/download?image=';
  static const String DownloadIMG =
      'http://bgs098.zucco.tech:8080/api/file/v1/image/download?image=';
  static const String WebSocketURL = 'ws://bgs098.zucco.tech:8004/ws';
}
