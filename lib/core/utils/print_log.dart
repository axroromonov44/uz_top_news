import 'package:flutter/foundation.dart';

void printLog(dynamic child) {
  if (kDebugMode) {
    print(child.toString());
  }
}
