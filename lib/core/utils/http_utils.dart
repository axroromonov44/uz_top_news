import 'dart:convert';

import 'package:http/http.dart';
import 'package:uz_top_news/core/utils/print_log.dart';

import '../app_global_state.dart';
import '../error/exception.dart';
import 'constants/hive_const_keys.dart';

enum UploadType { image, video }

// ignore: avoid_classes_with_only_static_members
class HttpUtil {
  static Future<Response> request(String url,
      {Map<String, String>? headers,
        dynamic body,
        required Function requestType}) async {
    try {
      printLog(url);
      printLog("Request body: ${body ?? ''}");

      final httpHeaders = <String, String>{
        'Content-Type': 'application/json',
        'Authorization': AppGlobalState.hive
            .get(HiveKeys.authorization, defaultValue: '') as String
      };

      if (headers != null) httpHeaders.addAll(headers);

      dynamic response;

      if (requestType == get || requestType == delete) {
        response = await requestType(Uri.parse(url), headers: httpHeaders);
      } else {
        response = await requestType(Uri.parse(url),
            headers: httpHeaders, body: jsonEncode(body));
      }

      printLog("Response body:  ${response.body}");

      return response as Response;
    } catch (e) {
      printLog(
          "Error caught in request() method inside http_utils.dart file:\n $e");
      throw const ServerException(-1);
    }
  }

  static Future<Response> uploadMultiPart(
      ///'image', 'file', 'video;
      String uploadingDataType,
      String url,
      String filePath,
      ) async {
    printLog(url);

    try {
      final request = MultipartRequest('POST', Uri.parse(url))
        ..files.add(await MultipartFile.fromPath(
          uploadingDataType,
          filePath,
        ));

      final response = await Response.fromStream(await request.send());

      printLog("Response StatusCode: ${response.statusCode}");
      printLog("Response body: ${response.body}");

      return response;
    } catch (e) {
      printLog(
          "Error caught in uploadMultiPart() method inside http_utils.dart file\n $e");

      throw const ServerException(-1);
    }
  }
}