
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:uz_top_news/core/translations/locale_keys.g.dart';

import '../config/theme/style.dart';

abstract class ToastUtils {
  /// show toast message
  static void showShortToast(BuildContext context, String message,
      [Color? color]) {
    dismissToast(context);
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: color ?? Colors.black.withOpacity(0.9),
        duration: const Duration(seconds: 3),
        content: Row(
          children: [
            Flexible(
              child: Text(message,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: kMediumBlack16.copyWith(color: Colors.white)),
            ),
          ],
        ),
      ),
    );
  }

  ///used to show SnackBar above the bottomNavBar
  static void showToastWithPadding(BuildContext context, String message,
      [Color? color]) {
    dismissToast(context);
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        padding: const EdgeInsets.all(16),
        margin: EdgeInsets.only(
            bottom: kBottomNavigationBarHeight +
                ((SizerUtil.deviceType == DeviceType.tablet) ? 2.5.h : 0),
            left: 10.0,
            right: 10.0),
        behavior: SnackBarBehavior.floating,
        backgroundColor: color ?? const Color.fromRGBO(0, 0, 0, 0.6),
        duration: const Duration(seconds: 3),
        content: Row(
          children: [
            const Image(
              image: AssetImage('assets/icons/tick.png'),
              color: kWhite,
            ),
            Text(message, style: kMediumBlack16.copyWith(color: Colors.white)),
          ],
        ),
      ),
    );
  }

  // static Future<void> showAlertDialog(BuildContext context, String message,
  //     String buttonText, VoidCallback onTap,
  //     [Color? color]) async {
  //   await showDialog(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           content: Text(message),
  //           actions: [
  //             MaterialButton(
  //               onPressed: () {
  //                 Navigator.of(context).pop();
  //               },
  //               child: Text(LocaleKeys.cancel.tr()),
  //             ),
  //             MaterialButton(
  //               onPressed: () {
  //                 Navigator.pop(context);
  //                 onTap();
  //               },
  //               child: Text(
  //                 buttonText,
  //                 style: TextStyle(color: Theme.of(context).primaryColor),
  //               ),
  //             ),
  //           ],
  //         );
  //       });
  // }

  /// show error message [SnackBar]
  static void showErrorMessage(BuildContext context, String message,
      String buttonText, VoidCallback onTap,
      [Color? color]) {
    dismissToast(context);
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: color ?? Colors.black.withOpacity(0.8),
        duration: const Duration(seconds: 3),
        content: Row(
          children: [
            Text(message, style: kMediumBlack14.copyWith(color: Colors.white)),
            const Spacer(),
            InkWell(
              onTap: () async {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
                onTap();
              },
              child: Text(
                buttonText,
                style: kMediumBlack14.copyWith(
                    color: Theme.of(context).primaryColor),
              ),
            ),
          ],
        ),
      ),
    );
  }

  static void showNoInternetSnackBar(BuildContext context) =>
      ToastUtils.showShortToast(context, LocaleKeys.check_internet.tr());

  static void dismissToast(BuildContext context) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
  }
}
