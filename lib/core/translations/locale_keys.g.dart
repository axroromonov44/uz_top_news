// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const locale_full = 'locale_full';
  static const locale_short = 'locale_short';
  static const language = 'language';
  static const darkMode = 'darkMode';
  static const clearCache = 'clearCache';
  static const privacyPolicy = 'privacyPolicy';
  static const refresh = 'refresh';
  static const noConnection = 'noConnection';
  static const check_internet = 'check_internet';
  static const server_failure = 'server_failure';
  static const cache_failure = 'cache_failure';
  static const phone_number_already_in_use = 'phone_number_already_in_use';
  static const phone_number_not_found = 'phone_number_not_found';
  static const wrong_code_verification = 'wrong_code_verification';
  static const code_verification_expired = 'code_verification_expired';
  static const social_auth_cancelled = 'social_auth_cancelled';
  static const error_upload_image = 'error_upload_image';
  static const error_edit_user_info = 'error_edit_user_info';
  static const error_loading_user_data = 'error_loading_user_data';
  static const errorOccurred = 'errorOccurred';
  static const error = 'error';
  static const email_already_exists = 'email_already_exists';
  static const login = 'login';
  static const waiting_payment = 'waiting_payment';
  static const ready_orders = 'ready_orders';
  static const sent_orders = 'sent_orders';
  static const delivered_orders = 'delivered_orders';
  static const shouldNotBeEmpty = 'shouldNotBeEmpty';
  static const mainAddress = 'mainAddress';
  static const Edit = 'Edit';
  static const Home = 'Home';

}
