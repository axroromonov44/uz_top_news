// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en_US = {
  "locale_full": "English",
  "locale_short": "en",
  "language": "Language",
  "darkMode": "Dark mode",
  "clearCache": "Clear cache",
  "privacyPolicy": "Privacy policy",
  "refresh": "Refresh",
  "noConnection": "No Internet Connection",
  "check_internet": "Please, check your internet connection",
  "server_failure": "Error occur while connecting to server",
  "cache_failure": "Error occur while connecting to database",
  "phone_number_already_in_use": "Phone Number Already In Use",
  "phone_number_not_found": "Phone Number Not Found",
  "wrong_code_verification": "Wrong Code Verification",
  "code_verification_expired": "Code Verification Expired",
  "social_auth_cancelled": "Auth cancelled",
  "error_upload_image": "Rasm/dokument yuklashda xatolik yuz berdi",
  "error_edit_user_info": "Ma'lumotlarni tahrirlashda xatolik yuz berdi",
  "error_loading_user_data": "Foydalanuvchi ma'lumotlarini yuklashda xatolik yuz berdi",
  "errorOccurred": "Xatolik yuz berdi",
  "error": "Xatolik",
  "email_already_exists": "email_already_exists",
  "login": "Login",
  "waiting_payment": "Waiting payment",
  "ready_orders": "Ready orders",
  "sent_orders": "Sent orders",
  "delivered_orders": "Delivered orders",
  "shouldNotBeEmpty": "This field should not be empty",
  "mainAddress": "Main address",
  "Edit": "Edit",
  "Home": "Home"
};
static const Map<String,dynamic> ru_RU = {
  "locale_full": "Русский",
  "locale_short": "ru",
  "language": "Язык",
  "darkMode": "Темный режим",
  "clearCache": "Очистить кэш",
  "privacyPolicy": "Политика конфиденциальности",
  "refresh": "Обновить",
  "noConnection": "Нет интернет соединения",
  "check_internet": "Пожалуйста,проверьте соединение к сети",
  "server_failure": "Error occur while connecting to server",
  "cache_failure": "Error occur while connecting to database",
  "phone_number_already_in_use": "Phone Number Already In Use",
  "phone_number_not_found": "Phone Number Not Found",
  "wrong_code_verification": "Wrong Code Verification",
  "code_verification_expired": "Code Verification Expired",
  "social_auth_cancelled": "Auth cancelled",
  "error_upload_image": "Ошибка при загрузке фото/документа",
  "error_edit_user_info": "Ошибка при редактировании данных",
  "error_loading_user_data": "Ошибка при загрузке данных пользователя",
  "errorOccurred": "Произошла ошибка",
  "error": "Ошибка",
  "email_already_exists": "email_already_exists",
  "login": "Войти",
  "waiting_payment": "Waiting payment",
  "ready_orders": "Ready orders",
  "sent_orders": "Sent orders",
  "delivered_orders": "Delivered orders",
  "shouldNotBeEmpty": "This field should not be empty",
  "mainAddress": "Main address",
  "Edit": "Редактировать",
  "Home": "Home"
};
static const Map<String,dynamic> uz_UZ = {
  "locale_full": "O'zbekcha",
  "locale_short": "uz",
  "language": "Language",
  "darkMode": "Dark mode",
  "clearCache": "Clear cache",
  "privacyPolicy": "Privacy policy",
  "refresh": "Refresh",
  "noConnection": "Internet aloqasi yo'q",
  "check_internet": "Iltimos, internet aloqangizni tekshirib ko'ring",
  "server_failure": "Error occur while connecting to server",
  "cache_failure": "Error occur while connecting to database",
  "phone_number_already_in_use": "Phone Number Already In Use",
  "phone_number_not_found": "Phone Number Not Found",
  "wrong_code_verification": "Wrong Code Verification",
  "code_verification_expired": "Code Verification Expired",
  "social_auth_cancelled": "Auth cancelled",
  "error_upload_image": "Error occured while uploading image",
  "error_edit_user_info": "Error occured while editing user info",
  "error_loading_user_data": "Error occured while loading user data",
  "errorOccurred": "Error occured",
  "error": "Error",
  "email_already_exists": "email_already_exists",
  "login": "Kirish",
  "waiting_payment": "Waiting payment",
  "ready_orders": "Ready orders",
  "sent_orders": "Sent orders",
  "delivered_orders": "Delivered orders",
  "shouldNotBeEmpty": "This field should not be empty",
  "mainAddress": "Main address",
  "Edit": "Tahrirlash",
  "Home": "Uy"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en_US": en_US, "ru_RU": ru_RU, "uz_UZ": uz_UZ};
}
