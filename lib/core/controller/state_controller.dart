import 'dart:async';

import 'package:rxdart/rxdart.dart';

class StateController<T> {
  final _streamController = BehaviorSubject<T>();

  Stream<T> get currentStateStream => _streamController.stream;

  StreamSink<T> get updateState => _streamController.sink;

  void dispose() => _streamController.close();
}
